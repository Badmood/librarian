import db from '../db/db';
class BooksController{

    getAllBooks(req,res){
        res.status(200).send({
            success: 'true',
            message: 'books retrieved successfully',
            books: db
        })
    }

    getBook(req,res){
        const id = parseInt(req.params.id, 10);
        db.map((book) => {
            if (book.id === id) {
                return res.status(200).send({
                success: 'true',
                message: 'book retrieved successfully',
                book,
                });
            } 
        });
        return res.status(404).send({
            success: 'false',
            message: 'book does not exist',
        });
    }

    createBook(req,res){
        if(!req.body.title) {
            return res.status(400).send({
                success: 'false',
                message: 'title is required'
            });
        } else if(!req.body.description) {
            return res.status(400).send({
                success: 'false',
                message: 'description is required'
            });
        }
        const book = {
            id: db.length + 1,
            title: req.body.title,
            description: req.body.description
        }
        db.push(book);
        return res.status(201).send({
            success: 'true',
            message: 'book added successfully',
            book
        })
    }

    updateBook(req,res){
        const id = parseInt(req.params.id, 10);
        let bookFound;
        let itemIndex;
        db.map((book, index) => {
            if (book.id === id) {
                bookFound = book;
                itemIndex = index;
            }
        });
    
        if (!bookFound) {
            return res.status(404).send({
                success: 'false',
                message: 'book not found',
            });
        }
    
        if (!req.body.title) {
            return res.status(400).send({
                success: 'false',
                message: 'title is required',
            });
        } else if (!req.body.description) {
            return res.status(400).send({
                success: 'false',
                message: 'description is required',
            });
        }
    
        const updatedBook = {
            id: bookFound.id,
            title: req.body.title || bookFound.title,
            description: req.body.description || bookFound.description,
        };
    
        db.splice(itemIndex, 1, updatedBook);
    
        return res.status(201).send({
            success: 'true',
            message: 'book added successfully',
            updatedBook,
        });
    }

    deleteBook(req,res){
        const id = parseInt(req.params.id, 10);

        db.map((book, index) => {
            if (book.id === id) {
                db.splice(index, 1);
                return res.status(200).send({
                    success: 'true',
                    message: 'Book deleted successfuly',
                });
            }
        });


        return res.status(404).send({
            success: 'false',
        message: 'todo not found',
        });
    }

}

const booksController = new BooksController();
export default booksController;