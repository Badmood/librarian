import express from 'express';
import booksController from '../controllers/books';

const router = express.Router();

router.get('/api/books',booksController.getAllBooks);
router.get('/api/books/:id',booksController.getBook);
router.post('/api/books/add',booksController.createBook);
router.put('/api/books/update/:id',booksController.updateBook);
router.delete('/api/books/delete/:id',booksController.deleteBook);

export default router;